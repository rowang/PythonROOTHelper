// dependencies inside ROOT
#include <TH1D.h>
#include <TH2D.h>

// dependencies outside package 
#include "AsgTools/AsgTool.h"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace MyHistHelper
{
  // using namespace boost::numeric::ublas;
  void NormalizePerX(TH2D* );
  void NormalizePerY(TH2D* );
  void EqualBin1D(TH1D *&);
  void EqualBin2D(TH2D *&);
  void MyRebin2D(
      TH2D *&th2, Int_t nxbins, const Double_t *xbins, Int_t nybins, 
       const Double_t *ybins, const char* newname);
  void MyRebin2D(
      TH2D *&th2, Int_t nxbins, const Double_t *xbins, Int_t nybins, 
       const Double_t *ybins);
  void MyRebin(TH1D*&, Int_t , const Double_t *);
  inline double square( double root );
}
