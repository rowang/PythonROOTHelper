// dependencies in c__
#include <iostream>
#include <math.h>
#include <sstream>

// dependencies inside ROOT
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TMath.h>


namespace MyHistHelper
{

  void NormalizePerX(TH2D* h)
  {
    auto NbinsX = h->GetNbinsX();
    auto NbinsY = h->GetNbinsY();
    for (int binX = 1; binX <= NbinsX; ++binX)
    {
      auto InteSum = h->Integral(binX, binX, 1, NbinsY);
      std::cout << "The Sum Of Integral is " << InteSum << std::endl;
      for (int binY = 1; binY <= NbinsY; ++binY)
      {
        h->SetBinContent(binX, binY, 
            h->GetBinContent(binX, binY) / InteSum);
      }
    }
  }

  void NormalizePerY(TH2D* h)
  {
    auto NbinsX = h->GetNbinsX();
    auto NbinsY = h->GetNbinsY();
    for (int binY = 1; binY <= NbinsY; ++binY)
    {
      auto InteSum = h->Integral(1, NbinsX, binY, binY);
      for (int binX = 1; binX <= NbinsX; ++binX)
      {
        h->SetBinContent(binX, binY, 
            h->GetBinContent(binX, binY) / InteSum);
      }
    }
  }

  void EqualBin1D(TH1D *&th1)
  {
    std::string oldname = std::string(th1->GetName());
    std::string newname = std::string(oldname+"_1");
    Int_t nbinx = th1->GetNbinsX();
    TH1D *hnew = new TH1D(newname.c_str(), oldname.c_str(), 
        nbinx, 0, nbinx);
    for (int binX = 1; binX <= nbinx; ++binX)
    {
      hnew->SetBinContent(binX, th1->GetBinContent(binX));
    }
    th1 = (TH1D*)gDirectory->Get(newname.c_str());
    gDirectory->Delete(oldname.c_str());
    th1->SetName(oldname.c_str());
  }

  void EqualBin2D(TH2D *&th2)
  {
    std::string oldname = std::string(th2->GetName());
    std::string newname = std::string(oldname+"_1");
    Int_t nbinx = th2->GetNbinsX();
    Int_t nbiny = th2->GetNbinsY();
    TH2D *hnew = new TH2D(newname.c_str(), oldname.c_str(), 
        nbinx, 0, nbinx,
        nbiny, 0, nbiny);
    for (int binX = 1; binX <= nbinx; ++binX)
    {
      for (int binY = 1; binY <= nbiny; ++binY)
      {
        hnew->SetBinContent(binX, binY, th2->GetBinContent(binX, binY));
      }
    }
    th2 = (TH2D*)gDirectory->Get(newname.c_str());
    gDirectory->Delete(oldname.c_str());
    th2->SetName(oldname.c_str());
  }

  void MyRebin(TH1D*& th1, Int_t nxbins, const Double_t *xbins)
  {
    std::string oldname = std::string(th1->GetName());
    std::string newname = std::string(oldname+"_1");
    th1->Rebin(nxbins, newname.c_str() ,xbins);
    th1 = (TH1D*)gDirectory->Get(newname.c_str());
    gDirectory->Delete(oldname.c_str());
    th1->SetName(oldname.c_str());
  }

  inline double square(double root)
  {
    return root*root;
  }

  void MyRebin2D(
      TH2D *&th2, Int_t nxbins, const Double_t *xbins, Int_t nybins, 
       const Double_t *ybins, const char* newname)
  {
    TH2D *hnew = new TH2D(newname, newname, nxbins, 
        xbins, nybins, ybins);
    // old bin numbers
    Int_t nbinsx_old = th2->GetNbinsX();
    Int_t nbinsy_old = th2->GetNbinsY();
    auto fXaxis = th2->GetXaxis();
    auto fYaxis = th2->GetYaxis();
    Int_t startbinx = 1;
    const Double_t newxmin = hnew->GetXaxis()->GetBinLowEdge(1);
    // make sure the starting bin number of the old hist for merging.
    while( fXaxis->GetBinCenter(startbinx) < newxmin && 
        startbinx <= nbinsx_old ) 
    {
      startbinx++;
    }
    Int_t startbiny = 1;
    const Double_t newymin = hnew->GetYaxis()->GetBinLowEdge(1);
    while( fYaxis->GetBinCenter(startbiny) < newymin && 
        startbiny <= nbinsy_old ) 
    {
      startbiny++;
    }
    Double_t binContent, binError;
    Int_t oldbinx, oldbiny;
    // old bins are the bins you will start counting. 
    // because we're rebinning along y direction, oldbinx will be 
    // increasing, but oldbiny will return to start when oldbinx
    // gets increased
    oldbinx=startbinx;
    oldbiny=startbiny;
    Int_t i,j;
    for (int binx = 1; binx <= nxbins; binx++)
    {
      // binx and biny are new bins
      Int_t imax = nbinsx_old;
      Double_t xbinmax = hnew->GetXaxis()->GetBinUpEdge(binx);
      oldbiny=startbiny;
      for (int biny = 1; biny <= nybins; biny++)
      {
        //cout << "y" << biny << " ";
        binContent = 0;
        binError   = 0;
        Int_t jmax = nbinsy_old;
        Double_t ybinmax = hnew->GetYaxis()->GetBinUpEdge(biny);
        // these are itrate through old bins
        for (i=0;i<nbinsx_old;i++) 
        {
          if( ( fXaxis->GetBinCenter(oldbinx+i) > xbinmax))  
          {
            // break happens when the bin goes beyond the new bin upedge
            imax = i;
            break;
          }
          for (j=0;j<nbinsy_old;j++) {
            if(  (fYaxis->GetBinCenter(oldbiny+j) > ybinmax) ) {
              jmax = j;
              break;
            }
            binContent += th2->GetBinContent(oldbinx+i,oldbiny+j);
            binError += square(th2->GetBinError(oldbinx+i,oldbiny+j));
          }
        }
        hnew->SetBinContent(binx,biny,binContent);
        hnew->SetBinError(binx,biny,TMath::Sqrt(binError));
        // at the break, oldbin + max will be the index of the 
        // first old bin for next newbin
        oldbiny += jmax;
      }
      oldbinx += imax;
    }

    // set under/over flow along x-axis in order
    for (int binx = 0; binx <= nxbins+1; binx++)
    {
      Int_t Oldbinx=0;
      binContent = 0;
      binError = 0;
      Double_t xbinmax = hnew->GetXaxis()->GetBinUpEdge(binx);
      Int_t imax = nbinsx_old;
      for (i=0; i<nbinsx_old; i++) 
      {
        if( ( fXaxis->GetBinCenter(Oldbinx+i) > xbinmax))  
        {
          imax = i;
          break;
        }
        for (j=0; j<startbiny; j++) {
          binContent += th2->GetBinContent(Oldbinx+i,j);
          binError += square(th2->GetBinError(Oldbinx+i,j));
        }
      }
      hnew->SetBinContent(binx, 0, binContent);
      hnew->SetBinError(binx, 0, TMath::Sqrt(binError));
      binContent = 0;
      binError = 0;
      for (i=0; i<nbinsx_old; i++) 
      {
        if( ( fXaxis->GetBinCenter(Oldbinx+i) > xbinmax))  
        {
          imax = i;
          break;
        }
        for (j=oldbiny; j<=nbinsy_old+1; j++) {
          binContent += th2->GetBinContent(Oldbinx+i,j);
          binError += square(th2->GetBinError(Oldbinx+i,j));
        }
      }
      hnew->SetBinContent(binx, nybins+1, binContent);
      hnew->SetBinError(binx, nybins+1, TMath::Sqrt(binError));
      Oldbinx += imax;
    }

    // set under/over flow along y-axis in order
    for (int biny = 0; biny <= nybins+1; biny++)
    {
      Int_t Oldbiny=0;
      binContent = 0;
      binError = 0;
      Int_t jmax = nbinsy_old;
      Double_t ybinmax = hnew->GetXaxis()->GetBinUpEdge(biny);
      for (j=0; j<nbinsy_old; j++) 
      {
        if( ( fYaxis->GetBinCenter(Oldbiny+j) > ybinmax))  
        {
          jmax = j;
          break;
        }
        for (i=0; i<startbinx; i++) {
          binContent += th2->GetBinContent(i,Oldbiny+j);
          binError += square(th2->GetBinError(i,Oldbiny+j));
        }
      }
      hnew->SetBinContent(0, biny, binContent);
      hnew->SetBinError(0, biny, TMath::Sqrt(binError));
      binContent = 0;
      binError = 0;
      for (j=0; j<nbinsy_old; j++) 
      {
        if( ( fYaxis->GetBinCenter(Oldbiny+j) > ybinmax))  
        {
          jmax = j;
          break;
        }
        for (i=oldbinx; i<=nbinsx_old+1; i++) {
          binContent += th2->GetBinContent(i,Oldbiny+j);
          binError += square(th2->GetBinError(i,Oldbiny+j));
        }
      }
      hnew->SetBinContent(nxbins+1, biny, binContent);
      hnew->SetBinError(nxbins+1, biny, TMath::Sqrt(binError));
      Oldbiny += jmax;
    }
    // if(newname == "")
    // {
      // delete th2;
      // th2 = hnew;
    // }
  }

  void MyRebin2D(
      TH2D *&th2, Int_t nxbins, const Double_t *xbins, 
      Int_t nybins, const Double_t *ybins)
  {
    // not sure if gDirectory Get will get the correct object
    // from correct realm. In the current use-case, there's no ambiguity.
    std::string oldname = std::string(th2->GetName());
    std::string newname = std::string(oldname+"_1");
    MyRebin2D(th2, nxbins, xbins, nybins, ybins, newname.c_str() );
    //hist_temp->Rebin(N_rebin, newname.c_str() ,binningx); 
    th2 = (TH2D*)gDirectory->Get(newname.c_str());
    gDirectory->Delete(oldname.c_str());
    th2->SetName(oldname.c_str());
  }

}// endo of namespace
