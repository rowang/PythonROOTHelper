#!/usr/bin/env python

import math

def GetSumOfError1D(hist):
    toterr = 0.
    for bini in xrange(hist.GetNbinsX()):
        # the underflow is 0,  the overflow is n+1, so modify the index
        bini += 1
        # GetBinError is square of sum of square weight in that bin, add them up.
        toterr += hist.GetBinError(bini) ** 2
    return math.sqrt(toterr)


# test suit
